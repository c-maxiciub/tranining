**Using gitlab runners to do various build/test/deploy stages** <br>
The *build* and *test* job both run on $GITLAB_RUNNERS_DEV,<br>

 [link to .build.yml](https://gitlab.com/e3803/enr/enr/-/blob/master/cicdyml/.build.yml?ref_type=heads)
```yml
build:
 stage:  build
 image:  ennexus.corp.edifecs.com:5000/consumer/java-npm-build:11.0.15
 script:
 -  mvn  -T  4  -s  settings.xml  -f  source/pom.xml  clean  install  -U  -DmavenTestFailureIgnore=true  -DskipIntegrationTests=true  -DskipUnitTests=true
 tags:
 -  $GITLAB_RUNNERS_DEV 
```
The *deploy* job run on $GITLAB_RUNNERS_PROD
[link to .deploy_release.yml](https://gitlab.com/e3803/enr/enr/-/blob/master/cicdyml/.deploy_release.yml?ref_type=heads)
```yml
deploy_release:
 stage:  deploy
 image:  ennexus.corp.edifecs.com:5000/consumer/java-npm-build:11.0.15
 script:
 -  mvn  -s  settings.xml  -f  source/pom.xml  clean  -DBUILD_NUMBER=$CI_BUILD_NUMBER  --batch-mode  --errors  --fail-at-end  --show-version  -DinstallAtEnd=true  -DdeployAtEnd=true  -Dsha1=%CI_COMMIT_SHA%  deploy  -Prelease
 tags:
 -  $GITLAB_RUNNERS_PROD
 only:
 refs:
 -  tags
 -  /^.*-RELEASE.*$
```
----------------
**Overview of various Gitlab Scans and how to configure them in project**<br>
*SAST*, *DAST*, and *Dependency scanners* are different types of security testing tools used in the field of software development to identify and address potential security vulnerabilities in applications.
[link to results show vulnerability findings](https://gitlab.com/e3803/enr/enr-ui/enr-ui-frontend/-/pipelines/1186566611/security)
1.  **SAST (Static Application Security Testing):**
    -   **What:** Analyzes source code for vulnerabilities.
    -   **How:** Scans code without executing it.
    -   **Advantages:** Early detection, integrated into development.
2.  **DAST (Dynamic Application Security Testing):**
    -   **What:** Tests running applications for security flaws.
    -   **How:** Simulates real-world attacks during runtime.
    -   **Advantages:** Identifies runtime-specific vulnerabilities.
3.  **Dependency Scanners:**
    -   **What:** Analyzes third-party libraries for security issues.
    -   **How:** Checks dependencies against known vulnerability databases.
    -   **Advantages:** Manages risks in external components.
    [link to DAST.gitlab-ci.yml example](https://gitlab.com/e3803/enr/qa/jtest-acceptance/-/blob/88b8ca358e2c258e7580e5946f8218249df177fe/cicdyml/DAST.gitlab-ci.yml)
    [link to DAST Scan Authentication Report](https://e3803.gitlab.io/-/enr/qa/jtest-acceptance/-/jobs/6235175001/artifacts/gl-dast-debug-auth-report.html)

**Implement:**
```yml
include:
	- template: Security/Dependency-Scanning.gitlab-ci.yml
	- template: Security/SAST.gitlab-ci.yml
```

```yml
include:
 template:  DAST.gitlab-ci.yml 
dast:
 stage:  dast
 variables:
 DAST_WEBSITE:  "https://hostname:8443/enr/"
 DAST_AUTH_URL:  "https://hostname:8443/enr/logon/logon.jsp"
 DAST_USERNAME:  $DAST_USERNAME
 DAST_PASSWORD:  $DAST_PASSWORD
 DAST_USERNAME_FIELD:  "id:Username" 
 DAST_PASSWORD_FIELD:  "input[name=password]" 
 DAST_SUBMIT_FIELD:  "input[name=sign-in]" 
 DAST_MAX_RESPONSE_SIZE_MB:  "20"
 DAST_AUTH_REPORT:  "true"
 DAST_FULL_SCAN_ENABLED:  "true"
 DAST_BROWSER_SCAN:  "true"
 DAST_BROWSER_LOG:  "crawl:debug,brows:debug,auth:debug"
 tags:
 -  $GITLAB_RUNNERS_DEV
 artifacts:
	paths:  [gl-dast-debug-auth-report.html]
	 when:  always
```
